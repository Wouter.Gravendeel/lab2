package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;


    //String name1 = "Lugia";
    //int healthPoints1 = 500;
    //int strength1 = 100;
    //pokemon1 = new Pokemon(name1, healthPoints1, strength1);

    //String name2 = "Ho-oh";
    //int healthPoints2 = 500;
    //int strength3 = 100;
    //pokemon2 = new Pokemon(name2, healthPoints2, strength2);

    public static void main(String[] args) {
        pokemon1 = new Pokemon("Lugia", 500, 100);
        pokemon2 = new Pokemon("Ho-oh", 500, 100);

        int whoseTurn = 1;

        while (pokemon1.isAlive() && pokemon2.isAlive()){
            if (whoseTurn == 1){
                pokemon1.attack(pokemon2);
                whoseTurn = 2;
            }
            else {
                pokemon2.attack(pokemon1);
                whoseTurn = 1;
            }
        }
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
    }
}